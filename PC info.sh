#! /bin/bash

#cpu information
function cpu {
echo -e "\033[033m*-*-*-*-*PROCESSOR INFORMATION*-*-*-*-*\033[0m"
number_proc=`cat /proc/cpuinfo | grep -c "processor"`
echo "Processors      : $number_proc"
vendor_id=`cat /proc/cpuinfo | grep "vendor_id" | head -1`
echo "$vendor_id"
model_name=`cat /proc/cpuinfo | grep  "model name" | head -1`
echo "$model_name"
cpu_MHz=`cat /proc/cpuinfo | grep  "cpu MHz" | head -1`
echo "$cpu_MHz"
fpu=`cat /proc/cpuinfo | grep  "fpu" | head -1`
echo "$fpu"
}

#HDD information
function HDD {
echo -e "\033[031m*-*-*-*-*HDD INFORMATION*-*-*-*-*\033[0m"
echo "`lsblk -rn | grep "sd" | cut -d" " -f1,4`"
}

#RAM information
function RAM {
echo -e "\033[032m*-*-*-*-*RAM INFORMATION*-*-*-*-*\033[0m"
ram=`free -h | grep "Mem:"`
echo "Total Memory    :" `echo $ram | cut -d" " -f2`
echo "Used Memory     :" `echo $ram | cut -d" " -f3`
echo "Free Memory     :" `echo $ram | cut -d" " -f4`
}

#system information
function system {
echo -e "\033[035m*-*-*-*-*SYSTEM INFORMATION*-*-*-*-*\033[0m"
echo "`uname --all | cut -d" " -f1,3,4,12`"
}

#internet information
function Internet {
echo -e "\033[034m*-*-*-*-*INTERNET INFORMATION*-*-*-*-*\033[0m"
eth=`ifconfig | grep "eth"`
if [ -n "$eth" ]
then
echo "eth             : yes"
else
echo "eth             : no"
fi
wlan=`ifconfig | grep "wlan"`
if [ -n "$wlan" ]
then
echo "wlan            : yes"
else
echo "wlan            : no"
fi
}

#display information
function Display {
echo -e "\033[036m*-*-*-*-*DISPLAY INFORMATION*-*-*-*-*\033[0m"
echo `xrandr | head -n1 `
echo `xrandr | head -n2 | tail -n1 `
echo `xrandr | grep  "*"`
echo `xwininfo -root | grep "Width" `
echo `xwininfo -root | grep "Height" `
echo `xwininfo -root | grep "Depth" `
echo `xwininfo -root | grep "Corners" `
echo `xwininfo -root | grep "geometry"`
}

#Help function
function Help {
echo "If you want to get information Processors, type		-CPU"
echo "If you want to get information HDD, type		-HDD"
echo "If you want to get information RAM, type		-RAM"
echo "If you want to get information System, type		-SYSTEM"
echo "If you want to get information Internet, type		-INTERNET"
echo "If you want to get information Display, type		-DISPLAY"
echo "If you want to get all information,  type		--all"
}

if [ -z "$*" ]
then
Help
fi

for option in {$1,$2,$3,$4,$5,$6}
do
if [ $1 = "--all" ]
then
cpu
HDD
RAM
System
Internet
Display
elif [ $option = "-CPU" ]
then
cpu
elif [ $option = "-HDD" ]
then
HDD
elif [ $option = "-RAM" ]
then
RAM
elif [ $option = "-SYSTEM" ]
then
system
elif [ $option = "-INTERNET" ]
then
Internet
elif [ $option = "-DISPLAY" ]
then
Display
else
reset
Help
fi
done
