#! /bin/bash/
quantity=0
for ip in `seq 1 254`
do
IP="192.168.1.$ip"
X=`ping -c1 $IP|grep "1 received"`
if [ "$X" != "" ]
then
echo -e "\033[33;44mIP=$IP\033[0m"
quantity=`expr $quantity + 1`
fi
done
echo -e "\033[32;41mQuantity=$quantity\033[0m" 
